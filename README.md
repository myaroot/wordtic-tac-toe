# WordTic tac toe

A word guessing Game and Tic tac toe game all in one.

The player will have the opportunity to play a thrilling word guessing game and may also choose to pay tic tac toe a player two game.
Enjoy !
# Word-Guessing-Game
This is a c++ word guessing game.

In this game words are generated at random and  displayed to the screen.

Two positions will be kept blank & Then user will guesses that word.

If he/she guesses the correct word then he/she will get a 1 point.

This game is executed in Ubuntu OS.

# To run     :  ./WordTic

# Tic Tac Toe Game

It as a player two game.
A visual grid is displayed where the players plot their moves
1. The game is played on a grid that's 3 squares by 3 squares.

2. Player 1 is X, Player 2 is O. Players take turns putting their marks in empty squares.

3. The first player to get 3 of her marks in a row (up, down, across, or diagonally) is the winner.

4. When all 9 squares are full, the game is over. If no player has 3 marks in a row, the game ends in a tie.

# To run     :  ./WordTic

